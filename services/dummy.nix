{ config, pkgs, lib, ...}:

with lib;

let
  cfg = config.services.dummy;

in {
  imports = [];

  options.services.dummy = {
    enable = mkOption {
      type = types.bool;
      default = true;
      description = "Enable the dummy service";
    };
  };

  config = {

    systemd.services.dummy = {
      description = "A dummy service that does nothing";
      serviceConfig = {
        Type = "simple";
        ExecStart = "${pkgs.bash}/bin/bash -c 'sleep 10000000'";
      };
      wantedBy = [ "multi-user.target" ];
    };

    environment.systemPackages = [ pkgs.coreutils pkgs.bash ];
  };

}

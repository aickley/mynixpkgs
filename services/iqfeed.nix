{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.services.iqfeed; 

in {

  # Service options
  options.services.iqfeed = rec {
    wine = mkOption {
      type = types.package;
      default = pkgs.wine;
      defaultText = "pkgs.wine";
      description = "Wine package";
    };
    user = mkOption {
      type = types.str;
      default = "iqfeed";
      description = "Run under user";
    };
    display = mkOption {
      type = types.str;
      default = ":1.0";
      description = "X display to use";
    };
    runPath = mkOption {
      type = types.str;
      default = "/run/iqfeed/";
      description = "Working directory";
    };
    connectArgs = mkOption {
      type = types.str;
      description = "arguments to pass to iqconnect.exe"; 
    };
  };

  # Resulting configuration
  config = {
    users.extraUsers."${cfg.user}" = {
      home = "${cfg.runPath}";
      createHome = true;
      group = "users";
    };
    
    environment.systemPackages = [
      cfg.wine
      pkgs.iqfeed-bin
      pkgs.coreutils
      pkgs.inotify-tools
      pkgs.netcat-gnu
    ];
  
    systemd.services.iqfeed = let

      forwardLog = pkgs.writeText "forward-log.sh" ''
        POS=0
        echo "Following IQFeed log at $1..." | systemd-cat -t iqfeed

        while [ 1 ]
        do
          SIZE=`wc -c < $1`
          dd skip=$POS count=$((SIZE-POS)) bs=1 if=$1 2>/dev/null | cut -f6 | systemd-cat -t iqfeed
          POS=$SIZE
          # Wait for the file to change. Suppress diagnostic and event info.
          inotifywait -q -e modify -e close_write $1 1>/dev/null 2>&1
        done
      '';

      waitAndListenQuotes = pkgs.writeText "wait-and-listen-quotes.sh" ''
        sleep 10
        echo "Connecting to the quotes port" | systemd-cat -t iqfeed
        netcat 127.0.0.1 5009 >/dev/null 2>&1
        echo "Connection to the quotes port lost" | systemd-cat -t iqfeed
      '';

      execStartPreScript = pkgs.writeText "exec-start-pre.sh" ''
        rm -rf ${cfg.runPath}
        mkdir -p ${cfg.runPath}/
        mkdir -p ${cfg.runPath}/DTN 
        touch ${cfg.runPath}/DTN/IQConnectLog.txt
        chown -R ${cfg.user} ${cfg.runPath}
      '';

      execStartScript = pkgs.writeText "exec-start.sh" ''
        wineboot -i | systemd-cat -t iqfeed/wine
        cp -r --update ${pkgs.iqfeed-bin}/drive_c ${cfg.runPath}
        /bin/sh ${forwardLog} ${cfg.runPath}DTN/IQConnectLog.txt >&2 &
        /bin/sh ${waitAndListenQuotes} &
        wine "C:\\iqfeed\\iqconnect.exe" ${cfg.connectArgs} | systemd-cat -t iqfeed/wine
      '';

      execStopScript = pkgs.writeText "exec-stop-post.sh" ''
        wineboot -e | systemd-cat -t iqfeed/wine
      '';

    in {
      description = "IQFeed client";
      environment = {
        WINEPREFIX = "${cfg.runPath}";
        WINEDLLOVERRIDES = "mscoree="; # do not gui-prompt about mono
        DISPLAY = "${cfg.display}";
        WINEDEBUG = "-all"; # turn off lots of fixme messages
      };
      path = with pkgs; [ coreutils wine inotify-tools systemd netcat-gnu ];

      serviceConfig = {
        # Create the default wine settings
        ExecStartPre = "/bin/sh ${execStartPreScript}";
        ExecStart = "/bin/sh ${execStartScript}";
        ExecStop = "/bin/sh ${execStopScript}";
        User = "${cfg.user}";
        StandardError = "journal";
        PermissionsStartOnly = "true";
      };
      #wantedBy = [ "multi-user.target" ];
      requires = [ "Xvfb:1.0.service" ];
    };
  }; # end config
} # end module


# This module runs ‘x11vnc' for a given user on a virtual screen provided by ‘Xvfb'.
# The Xvfb instances persist across VNC sessions.

{ config, lib, pkgs, ... }:

with lib;

{

  # ssh -l ilya -L 5900:localhost:5900 localhost 'x11vnc -localhost -display 1'
  # x11vnc -rfbauth /home/ilya/.vnc/passwd -display :1 -ncache 

  config = {

    environment.systemPackages = [ pkgs.x11vnc ];

    # https://gist.github.com/drmalex07/333d8a88c4918954e8e4
    # https://serverfault.com/questions/146572/starting-x11vnc-remotely-when-x-server-is-already-running

    # systemd.sockets.vnc = {
    #  description = "VNC Server Socket";
    #  wantedBy = [ "sockets.target" ];
    #  # before = [ "multi-user.target" ];
    #  unitConfig.PartOf = "vnc.service";
    #  socketConfig.Accept = true;
    #  socketConfig.ListenStream = 5900;
    #};

    #ip route get 8.8.8.8 | grep src | cut -d ' ' -f 7

    systemd.services."vnc" = {
      description = "VNC server on a given X screen";
      serviceConfig = {
        ExecStart = "${pkgs.x11vnc}/bin/x11vnc -display :1.0 -rfbport 5900";
        # vncserver :3 AcceptPointerEvents=false 
        Restart = "always";
        # ExecStart = "${pkgs.coreutils}/echo -display :1.0 -viewonly";
        StandardError = "journal";
        # Don't kill the X server when the user quits the VNC
        # connection.  FIXME: the X server should run in a
        # separate systemd session.
        KillMode = "process";
      };
    };

  };

}

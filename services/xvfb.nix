# This module creates a virtual X screen using Xvfb.

{ config, lib, pkgs, ... }:

with lib;

let cfg = config.services.xvfb; in 

{

  # Service options

  options.services.xvfb = {

    display = mkOption {
      type = types.int;
      default = 1;
      description = "X display number";
    };

    screen = mkOption {
      type = types.int;
      default = 0;
      description = "X screen number";
    };

    geometry = mkOption {
      type = types.str;
      default = "600x600x16";
      description = "Screen geometry as in 600x600x16";
    };

    userName = mkOption {
      type = types.str;
      default = "ilya";
      description = "Run under user";
    };
  };

  # Resulting configuration

  config = let

    Xvfb = "${pkgs.xvfb}/bin/Xvfb";
    rm = "${pkgs.coreutils}/bin/rm";
    dis = toString cfg.display;
    scr = toString cfg.screen;
    geom = cfg.geometry;

  in {

    #services.xserver.enable = true;
    #services.xserver.videoDrivers = [];
    users.extraUsers."${cfg.userName}" = {};
    environment.systemPackages = with pkgs; [ xvfb coreutils ];

    systemd.services."Xvfb:${dis}.${scr}" = {
      description = "X virtual framebuffer on display ${dis} and screen ${scr}";
      #path = [ pkgs.xorg.xorgserver.out ];
      serviceConfig = {
        ExecStart = "${Xvfb} :${dis} -screen ${scr} ${geom} -ac";
        ExecStopPost = "${rm} -f /tmp/.X${dis}-lock /tmp/.X11-unix/X${dis}"; # On fails too
        StandardError = "journal";
        KillMode = "process";
      };
    };

  };

}

# Returns a set of packages that will override those in the main channel.
#
# Use from global NixOS configuration via
#
#   nixpkg.config.packageOverrides = (import ./path);
#
# or from ~/.config/nixpkgs/config.nix via
#
#   packageOverrides = (import ./path);
#
# See
# http://nix-dev.science.uu.narkive.com/tEFAygDb/managing-private-nix-packages-outside-the-nixpkgs-tree
# http://sandervanderburg.blogspot.ch/2014/07/managing-private-nix-packages-outside.html

pkgs:

let
  callPackage = pkgs.lib.callPackageWith(pkgs // self);
  self = {
    hello = callPackage ./packages/hello.nix {};
    iqfeed-bin = callPackage ./packages/iqfeed-bin.nix {};
    xvfb = callPackage ./packages/xvfb.nix {};

    gce-image = callPackage ./packages/gce-image/default.nix {};

    gce-oslogin = callPackage ./packages/gce-oslogin.nix {};
    libjson-c = callPackage ./packages/libjson-c {};
  };

in self

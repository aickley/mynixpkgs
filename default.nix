# https://nixos.org/nix-dev/2016-December/022386.html
# https://nixos.org/nixpkgs/manual/#sec-overlays-definition
# https://github.com/NixOS/nixpkgs/issues/25264

import <nixpkgs> { overlays = [ (import ./overlays.nix) ]; }

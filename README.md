# Custom Nixkpgs

## Using from global configuration

Contains custom Nix packages and NixOS modules. Both can be used from the global
 `configuration.nix` via
```
  # Packages
  nixpkgs.config.packageOverrides = (import path/to/overrides.nix);

  # Modules 
  imports = [
    path/to/services/xvfb.nix
    # ...
  ];
```

## In-place building

One can test-build a package in place via
```
   nix-build -K -A xvfb .
```
It will be accessible `result` symlink

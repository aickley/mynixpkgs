# https://nixos.org/nix-dev/2016-December/022386.html
# https://nixos.org/nixpkgs/manual/#sec-overlays-definition
# https://github.com/NixOS/nixpkgs/issues/25264

self: super:
{
    myhello = super.callPackage ./packages/hello.nix {};
    iqfeed-bin = super.callPackage ./packages/iqfeed-bin.nix {};
    xvfb = super.callPackage ./packages/xvfb.nix {};
    gce-image = import ./packages/gce-image/build-image.nix { pkgs=self; };
}

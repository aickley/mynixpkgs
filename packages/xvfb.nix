{ stdenv, pkgs, runCommandCC }:

runCommandCC "xvfb" {

  name = "xvfb";

  meta = {
    description = "X server on virtual framebuffer";
  };

  nativeBuildInputs = with pkgs; [
    xorg.xorgserver
    stdenv
    coreutils
    gawk
    patchelf
  ];

} ''
  mkdir -p $out/bin $out/lib
  
  # Copy the executable
  BIN=${pkgs.xorg.xorgserver}/bin/.Xvfb-wrapped
  cp -pdv $BIN $out/bin/Xvfb
  
  # Copy the libraries
  # https://github.com/NixOS/nixpkgs/issues/30984
  LDD=$(${pkgs.glibc.bin}/bin/ldd $BIN)
  echo "$LDD" > $out/LDD
  LIBS="$(echo "$LDD" | awk '{print $3}' | sed '/^$/d')"
  echo "$LIBS" > $out/LIBS
  for LIB in $LIBS; do
    SRC=$(readlink -e $LIB)
    TGT=$out/lib/$(basename $LIB)
    cp -pdv "$SRC" "$TGT"
  done

  # Make the execuable use the copied libs
  chmod u+w $out/bin/Xvfb
  ${pkgs.patchelf}/bin/patchelf --set-rpath $out/lib $out/bin/Xvfb
  chmod a-w $out/bin/Xvfb
''

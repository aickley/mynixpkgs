{ stdenv
, lib
, fetchFromGitHub
, curl
, pam
, nss
, libjson-c
}:

stdenv.mkDerivation rec {
  name = "google-compute-engine-oslogin-${version}";
  version = "20171025";
  #namePrefix = "";

  src = fetchFromGitHub {
    owner = "GoogleCloudPlatform";
    repo = "compute-image-packages";
    rev = version;
    sha256 = "0pmf59c37bd8naycc3pclb6jp8yza817b6rsj4dd1n0lcm875d5k";
  };

  sourceRoot = "compute-image-packages-${version}-src/google_compute_engine_oslogin";

  propagatedBuildInputs = [ libjson-c pam curl.dev ];

  installFlags = [
    "DESTDIR=$(out)"
    "BIN_INSTALL_PATH=bin"
    "AUTHKEYS_INSTALL_PATH=bin"
  ];

  meta = with lib; {
    description = "Google Compute Engine tools and services";
    homepage = https://github.com/GoogleCloudPlatform/compute-image-packages;
    license = licenses.asl20;
    #maintainers = with maintainers; [ zimbatm ];
  };
}

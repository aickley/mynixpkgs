# https://theshortlog.com/2016/06/29/Prepare-NixOS-disk-image-for-another-computer/
{ pkgs ? import <nixpkgs> { overlays = [ (import ../../overlays.nix) ]; }
, lib ? pkgs.lib
, configFile ? ./image-config.nix
, diskSize ? 2048
}:
let
  config = (
    import <nixpkgs/nixos> { configuration = import configFile; }
  ).config;
in
  import <nixpkgs/nixos/lib/make-disk-image.nix> {
    inherit pkgs lib config configFile diskSize;
    partitioned = true;
    fsType = "ext4";
    postVM = ''
      PATH=$PATH:${pkgs.stdenv.lib.makeBinPath [ pkgs.gnutar pkgs.gzip ]}
      pushd $out
      mv $diskImage disk.raw
      # Pack the image without the empty space in the image
      tar -Szcf nixos-image-${config.system.nixosLabel}-${pkgs.stdenv.system}.raw.tar.gz disk.raw
      rm $out/disk.raw
      popd
    '';
  }

{ config, lib, pkgs, ... }:
let
  gce = pkgs.google-compute-engine;
in
{
  # NOTE: google-accounts tries to write to /etc/sudoers.d but the folder doesn't exist
  # FIXME: not such file or directory on dynamic SSH provisioning
  systemd.services.google-accounts-daemon = {
    description = "Google Compute Engine Accounts Daemon";
    # This daemon creates dynamic users
    enable = config.users.mutableUsers;
    after = [
      "network.target"
      "google-instance-setup.service"
      "google-network-setup.service"
    ];
    wantedBy = [ "multi-user.target" ];
    requires = ["network.target"];
    path = with pkgs; [ shadow ];
    serviceConfig = {
      Type = "simple";
      ExecStart = "${gce}/bin/google_accounts_daemon --debug";
    };
  };

  systemd.services.google-clock-skew-daemon = {
    description = "Google Compute Engine Clock Skew Daemon";
    after = [
      "network.target"
      "google-instance-setup.service"
      "google-network-setup.service"
    ];
    requires = [ "network.target" ];
    wantedBy = [ "multi-user.target" ];
    serviceConfig = {
      Type = "simple";
      ExecStart = "${gce}/bin/google_clock_skew_daemon --debug";
    };
  };

  systemd.services.google-instance-setup = {
    description = "Google Compute Engine Instance Setup";
    after = ["fs.target" "network-online.target" "network.target" "rsyslog.service"];
    before = ["sshd.service"];
    wants = ["local-fs.target" "network-online.target" "network.target"];
    wantedBy = [ "sshd.service" "multi-user.target" ];
    path = with pkgs; [ ethtool openssh ];
    serviceConfig = {
      ExecStart = "${gce}/bin/google_instance_setup --debug";
      Type = "oneshot";
    };
  };

  systemd.services.google-ip-forwarding-daemon = {
    description = "Google Compute Engine IP Forwarding Daemon";
    after = ["network.target" "google-instance-setup.service" "google-network-setup.service"];
    requires = ["network.target"];
    wantedBy = [ "multi-user.target" ];
    path = with pkgs; [ iproute ];
    serviceConfig = {
      Type = "simple";
      ExecStart = "${gce}/bin/google_ip_forwarding_daemon --debug";
    };
  };

  systemd.services.google-shutdown-scripts = {
    description = "Google Compute Engine Shutdown Scripts";
    after = [
      "local-fs.target"
      "network-online.target"
      "network.target"
      "rsyslog.service"
      "google-instance-setup.service"
      "google-network-setup.service"
    ];
    wants = [ "local-fs.target" "network-online.target" "network.target"];
    wantedBy = [ "multi-user.target" ];
    serviceConfig = {
      ExecStart = "${pkgs.coreutils}/bin/true";
      ExecStop = "${gce}/bin/google_metadata_script_runner --debug --script-type shutdown";
      Type = "oneshot";
      RemainAfterExit = true;
      TimeoutStopSec = 0;
    };
  };

  systemd.services.google-network-setup = {
    description = "Google Compute Engine Network Setup";
    after = [
      "local-fs.target"
      "network-online.target"
      "network.target"
      "rsyslog.service"
    ];
    wants = [ "local-fs.target" "network-online.target" "network.target"];
    wantedBy = [ "multi-user.target" ];
    serviceConfig = {
      ExecStart = "${gce}/bin/google_network_setup --debug";
      KillMode = "process";
      Type = "oneshot";
    };
  };

  systemd.services.google-startup-scripts = {
    description = "Google Compute Engine Startup Scripts";
    after = [
      "local-fs.target"
      "network-online.target"
      "network.target"
      "rsyslog.service"
      "google-instance-setup.service"
      "google-network-setup.service"
    ];
    wants = [ "local-fs.target" "network-online.target" "network.target"];
    wantedBy = [ "multi-user.target" ];
    serviceConfig = {
      ExecStart = "${gce}/bin/google_metadata_script_runner --debug --script-type startup";
      KillMode = "process";
      Type = "oneshot";
    };
  };

  # TODO: remove this
  systemd.services.fetch-ssh-keys =
    { description = "Fetch host keys and authorized_keys for root user";

      wantedBy = [ "sshd.service" ];
      before = [ "sshd.service" ];
      after = [ "network-online.target" ];
      wants = [ "network-online.target" ];

      script = let wget = "${pkgs.wget}/bin/wget --retry-connrefused -t 15 --waitretry=10 --header='Metadata-Flavor: Google'";
                   mktemp = "mktemp --tmpdir=/run"; in
        ''
          # When dealing with cryptographic keys, we want to keep things private.
          umask 077
          # Don't download the SSH key if it has already been downloaded
          echo "Obtaining SSH keys..."
          mkdir -m 0700 -p /root/.ssh
          AUTH_KEYS=$(${mktemp})
          ${wget} -O $AUTH_KEYS http://metadata.google.internal/computeMetadata/v1/project/attributes/sshKeys
          if [ -s $AUTH_KEYS ]; then

            # Read in key one by one, split in case Google decided
            # to append metadata (it does sometimes) and add to
            # authorized_keys if not already present.
            touch /root/.ssh/authorized_keys
            NEW_KEYS=$(${mktemp})
            # Yes this is a nix escape of two single quotes.
            while IFS=''' read -r line || [[ -n "$line" ]]; do
              keyLine=$(echo -n "$line" | cut -d ':' -f2)
              IFS=' ' read -r -a array <<< "$keyLine"
              if [ ''${#array[@]} -ge 3 ]; then
                echo ''${array[@]:0:3} >> $NEW_KEYS
                echo "Added ''${array[@]:2} to authorized_keys"
              fi
            done < $AUTH_KEYS
            mv $NEW_KEYS /root/.ssh/authorized_keys
            chmod 600 /root/.ssh/authorized_keys
            rm -f $KEY_PUB
          else
            echo "Downloading http://metadata.google.internal/computeMetadata/v1/project/attributes/sshKeys failed."
            false
          fi
          rm -f $AUTH_KEYS
        '';
      serviceConfig.Type = "oneshot";
      serviceConfig.RemainAfterExit = true;
      serviceConfig.StandardError = "journal+console";
      serviceConfig.StandardOutput = "journal+console";
    };

}

{ stdenv, fetchFromGitHub }:
let
  version = "0.12.1";
  date = "20160607";
  tag = "json-c-${version}-${date}";

in stdenv.mkDerivation rec {

  name = "libjson-c-${version}";

  src = fetchFromGitHub {
    owner = "json-c";
    repo = "json-c";
    rev = tag;
    sha256 = "05rzdbb6m4z3rh8cy8vi3h1cnnwj71j6669hz87y93l7vz08h255";
  };

  meta = {
    homepage = https://github.com/json-c/json-c/wiki;
    description = "A JSON implementation in C";
    longDescription = "JSON-C implements a reference counting object model that allows you to easily construct JSON objects in C, output them as JSON formatted strings and parse JSON formatted strings back into the C representation of JSON objects. It aims to conform to RFC 7159.";
    license = stdenv.lib.licenses.mit;
    platforms = stdenv.lib.platforms.unix;
  };

}

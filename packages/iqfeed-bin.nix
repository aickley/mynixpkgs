# https://github.com/NixOS/nixpkgs/blob/0604106b30c41e9b1f78810176f6145803515a93/pkgs/data/fonts/mph-2b-damase/default.nix

{ stdenv, fetchzip }:

fetchzip { 
  
  name = "iqfeed-wine-bin-5.2.6.0";

  # Direct download link is obtained as follows:
  # url = "https://www.dropbox.com/s/txon4pz9yrbw696/iqfeed-bin-5.2.6.0.zip?dl=0";
  url = "https://dl.dropboxusercontent.com/s/txon4pz9yrbw696/iqfeed-bin-5.2.6.0.zip";

  # A hash of the derivation, not the file itself
  sha256 = "0qz6ky6fbvf5qa474ys1jm0cg3ahyx72ilhgzyd9wy5simrvi4jm";

  postFetch = ''
    unzip $downloadedFile -d $out
  '';

}

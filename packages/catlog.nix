{ stdenv, pkgs, ... }:

stdenv.mkDerivation {

  name = "catlog";

  meta = {
    description = "";
  };

  catlogScript = pkgs.writeText "catlog.sh" ''
    #!/bin/sh
    POS=0
    echo Following $1...
    while [ 1 ]
    do
      SIZE=`wc -c < $1`
      dd skip=$POS count=$((SIZE-POS)) bs=1 if=$1 2>/dev/null | cut -f6 | systemd-cat -t iqfeed
      POS=$SIZE
      inotifywait -q -e modify -e close_write -t 5 $1
    done
  '';

}
